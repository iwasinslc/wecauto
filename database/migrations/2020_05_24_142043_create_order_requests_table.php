<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('main_currency_id')->nullable();
            $table->string('currency_id')->nullable();
            $table->string('main_wallet_id')->nullable();
            $table->string('wallet_id')->nullable();
            $table->integer('active')->default(1);
            $table->integer('type')->default(0);
            $table->string('user_id')->nullable();
            $table->float('amount', 16,8)->nullable();
            $table->float('free_amount', 16,8)->nullable();
            $table->float('rate', 16,8)->nullable();
            $table->dateTime('available_at');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_requests');
    }
}
