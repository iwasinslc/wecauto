<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\CyberwecRegisterNotification;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $data['email'] = strtolower($data['email']);
        $data['login'] = strtolower($data['login']);
        return Validator::make($data, [
            'name' => 'string|max:255',
            'phone' => 'string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'login' => 'required|string|max:30|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'partner_id' => 'required|exists:users,my_id',
//            'payment_code'=>'required|digits_between:6,10|numeric' ,
            'cyber_id' => request()->cyber_id!='' ? 'unique:users' : '',
            'captcha' =>  'required|captcha',
            'agreement' => 'required',
        ]);
    }

    /**
     * @param array $data
     * @return User
     * @throws \Throwable
     */
    protected function create(array $data)
    {

        $partner_id = $cyberId = null;

        if (isset($_COOKIE['partner_id'])) {
            $partner_id = $_COOKIE['partner_id'];
        } elseif (isset($data['partner_id'])) {
            $partner_id = $data['partner_id'];
        }

        if (isset($_COOKIE['cyber_id'])) {
            $cyberId = $_COOKIE['cyber_id'];
        } elseif (isset($data['cyber_id'])) {
            $cyberId = $data['cyber_id'];
        }

        if (empty($data['login'])) {
            $data['login'] = $data['email'];
        }

        $myId = generateMyId();

        /** @var User $user */
        $user = User::create([
            'name'       => $data['name'] ?? '',
            'email'      => strtolower($data['email']),
            'login'      => strtolower($data['login']),
            'phone'      => str_replace('@', '', $data['phone']),
            'password'   => bcrypt($data['password']),
            'partner_id' => $partner_id,
            'my_id'      => $myId,
            'cyber_id'   => $cyberId,
//            'payment_code'      => $data['payment_code'],
        ]);

        $user->fresh();

        $data = [
            'user' => [
                'name'          => $user->name,
                'email'         => $user->email,
                'referral_code' => null,
                'password'      => $data['password'],
//                'payment_code'      => $data['payment_code'],
            ]
        ];
        $user->sendNotification('registered',$data);

        if ($cyberId) {
            CyberwecRegisterNotification::dispatch($cyberId, $myId)
                ->onQueue(getSupervisorName() . '-high')
                ->delay(now());
        }

        return $user;
    }


    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        $partner = $user->partner;

        $message = $partner!==null&&!empty($partner->partner_text) ? $partner->partner_text : __('Congratulations, you have successfully registered.');

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('success', link_it($message));

    }
}
