<?php
namespace App\Rules;

use App\Models\Rate;
use App\Models\Setting;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class RuleCheckRate
 * @package App\Rules
 */
class RuleMaxPrice implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $max = 100000;

        $mainWallet = user()->wallets()->find(request()->main_wallet_id);


        if ($mainWallet->currency->code!='FST')
        {
            $max = (float) Setting::getValue('max_price_usd');
            if ($max==0) return true;
        }


        return $value<=$max;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Вы указали больше допустимой цены';
    }
}
