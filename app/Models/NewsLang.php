<?php
namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class NewsLang
 * @package App\Models
 *
 * @property string news_id
 * @property string lang_id
 * @property integer show
 * @property string title
 * @property string teaser
 * @property string text
 * @property string img
 * @property Carbon created_at
 */
class NewsLang extends Model
{
    use Uuids;
    use ModelTrait;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /** @var array $fillable */
    protected $fillable = [
        'news_id',
        'lang_id',
        'show',
        'title',
        'teaser',
        'text',
        'created_at',
        'img'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(News::class, 'news_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lang()
    {
        return $this->belongsTo(Language::class, 'lang_id');
    }


    public function addImg($file) {
        $folder          = 'news_img';
        $destinationPath = public_path() . '/'.$folder.'/';

        if (!empty($this->img)) {
            @unlink($destinationPath . $this->img);
        }

        $ext      = $file->getClientOriginalExtension() ?: 'png';
        $filename = str_random(20) . '.' . $ext;



        $path = $file->storeAs(
            'news', $filename, ['disk'=>'s3', 'visibility'=>'public']
        );

        $this->img = $path;
        $this->save();
    }

    public function getImgPath()
    {
        if ($this->img==null)
        {
            return '';
        }


        try
        {
            return Storage::disk('s3')->url($this->img);
        }
        catch (\Exception $e)
        {
            return '';
        }

    }


}
