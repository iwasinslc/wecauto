<?php
return [
    'order_created' =>'Order #:id :amount :currency was created successfully',
    'order_closed' => 'Order #:id :amount :currency was closed due to lack of funds on the balance sheet',
    'transfer_confirmed' => 'Transaction confirmed by link in the email',
    'withdraw_confirmed' => 'Withdraw confirmed by link in the email. Request was sent to administrator.',
    'sale' =>'Sale :amount :currency was successfully',
    'purchase'=>'Purchase :amount :currency was successfully',
    'partner_accrue'=>'You just got partner commission for :amount :currency from user :login, on level :level',
    'wallet_refiled'=>'Your wallet was refilled for :amount :currency',
    'rejected_withdrawal'=>'Your withdrawal for :amount :currency was cancelled.',
    'approved_withdrawal'=>'Your withdrawal for :amount :currency have been approved.',
    'new_partner'=>'You have a new partner :login at level :level',
    'parking_bonus'=>'Parking bonus :amount :currency',
    'licence_cash_back'=> 'Cashback for the purchase of a license :amount :currency'
];